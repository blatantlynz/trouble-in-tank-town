
var dgram = require('dgram');


function TankGamePlayer(id,x,y,theta){
  this.id = id;
  this.x = 0;
  this.y = 0;
  this.theta = theta;
}

function TankGame(){
  this.players = [];
  this.pendingActions = [];
}


TankGame.prototype.addPlayer = function(){
  var player = new TankGamePlayer(this.players.length,0,0,0);
  this.players.push(player);
};

TankGame.prototype.addAction = function(action){
  this.pendingActions.push(action);
};

TankGame.prototype.processAction = function(action){
  //TODO
};

TankGame.prototype.tick = function(){
  while(this.pendingActions.size > 0){
    var action = this.pendingActions.pop();
    this.processAction(action);
  }
};



function Server(){
  this.port = 1337;
  this.outPort = 1337;
  this.host = '127.0.0.1';
  this.server = dgram.createSocket('udp4');
  this.server.bind(this.port,this.host);
  var _this = this;
  this.addressIDs = {};
  this.nextID = 0;
  
  //Game Logic
  this.game = new TankGame();
  
  this.server.on('message',function(message,remote){
     _this.onMessage(message.toString(),remote); 
  });
  
  console.log("Listening for clients on port " + this.port);
  this.sendToAllClients();
}

//Calllback for when we get a message from a client
Server.prototype.onMessage = function(message,remote){
  //Get the id of the user

  if (this.getID(remote.address) == undefined){
      var id = this.setID(remote.address);
  }else{
      var id = this.getID(remote.address);
  }

  var messageObject = this.getMessageType(message,id);

  if (messageObject.action == "Echo"){
      this.echo(messageObject,remote);
  }else if (messageObject.action == "Connect"){
      this.onConnect(messageObject,remote);
  }

};


//Client Message Action
Server.prototype.onConnect = function(message,remote){
  console.log("Client " + message.id + " connected with ip: " + remote.address);
  var replyObject = {id:message.id,action:"Connect",message:"Success"};
  this.sendMessage(replyObject,remote);
};

//Client Message Action: Get other clients in lobby
Server.prototype.listPlayers = function(message,remote){
  var replyObject = {id:message.id,action:"Connect",message:"Success"};
  this.sendMessage(replyObject,remote);
}

//Client Message Action
Server.prototype.echo= function(message,remote){
  console.log(message.message,remote);
  this.sendMessage(message,remote);
}

Server.prototype.sendToAllClients = function(message){
  var remotes = Object.keys(this.addressIDs);
  var _this = this;
  remotes.forEach(function(remote,i){
    _this.sendMessage(message,remote);
  });
};

//Wrapper message to send data to a client
Server.prototype.sendMessage = function(messageObject,remote){
  var msgBuffer = new Buffer(this.formMessageString(messageObject));
  this.server.send(msgBuffer,0,msgBuffer.length,remote.port,remote.address,function(err,data){
      if (err){
          console.log(err);
      }else{
          console.log("reply sent");
      }
  });
}

Server.prototype.formMessageString = function(message){
  return JSON.stringify(message);
}

Server.prototype.getMessageType = function(message,id){
  //Determine the{ action for the message
  var _this = this;
  var msg = JSON.parse(message);
  msg.id = id;
  return msg;
}

Server.prototype.getID = function(addr){
  return this.addressIDs[addr];
}

Server.prototype.setID = function(addr){
  this.addressIDs[addr] = this.getNewID();
  return this.addressIDs[addr];
}

Server.prototype.getNewID = function(){
  var retVal = this.nextID;
  this.nextID++;
  return retVal;
}

var server = new Server();